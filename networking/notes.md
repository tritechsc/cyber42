PDU:
 Protocol Data Unit

ICMP:
Internet Control Message Protocol
From Wikipedia, the free encyclopedia
Jump to navigation
Jump to search
Internet Control Message ProtocolCommunication protocol
ICMP header - General-en.svg
A general header for ICMPv4
Purpose	Auxiliary protocol for IPv4[1]
Developer(s)	DARPA
Introduced	1981
OSI layer	Network layer
RFC(s)	RFC 792

The Internet Control Message Protocol (ICMP) is a supporting protocol in the Internet protocol suite. It is used by network devices, including routers, to send error messages and operational information indicating success or failure when communicating with another IP address, for example, when an error is indicated when a requested service is not available or that a host or router could not be reached.[2] ICMP differs from transport protocols such as TCP and UDP in that it is not typically used to exchange data between systems, nor is it regularly employed by end-user network applications (with the exception of some diagnostic tools like ping and traceroute). 

STP:
Spanning Tree Protocol (STP) is a Layer 2 protocol that runs on bridges and switches. The specification for STP is IEEE 802.1D. The main purpose of STP is to ensure that you do not create loops when you have redundant paths in your network. Loops are deadly to a network.

ARP:
Address Resolution Protocal
